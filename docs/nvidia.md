# Install the NVIDIA driver

1. Install the driver.

New:

```sh
yay -S nvidia-dkms nvidia-utils nvidia-settings opencl-nvidia vulkan-icd-loader
yay -S lib32-opencl-nvidia lib32-vulkan-icd-loader lib32-nvidia-utils
```

<= 390:

```sh
yay -S nvidia-390xx nvidia-390xx-settings nvidia-390xx-utils opencl-nvidia-390xx libvdpau libxnvctrl-390xx vulkan-icd-loader
yay -S lib32-nvidia-390xx-utils lib32-opencl-nvidia-390xx lib32-vulkan-icd-loader
```

2. Re-configure the system.

```sh
sudo nvim /etc/mkinitcpio.conf # Add "nvidia nvidia_modeset nvidia_uvm nvidia_drm" into MODULES
sudo nvim /etc/default-grub # Add "nvidia_drm.modeset=1" into GRUB_CMDLINE_LINUX
```

3. Configure the driver.

```sh
sudo mkdir -p /etc/pacman.d/hooks
cd ~/Downloads
curl -LO "https://gitlab.com/ArtLkv/my-preconfigure-arch/-/raw/etc/pacman.d/hooks/nvidia.hook"
sudo mv nvidia.hook /etc/pacman.d/hooks/nvidia.hook
sudo nvidia-xconfig
sudo mv /etc/X11/xorg.conf /etc/X11/xorg.conf.d/20-nvidia.conf

git clone https://aur.archlinux.org/nvidia-tweaks.git
cd nvidia-tweaks
makepkg -sric
sudo mkinitcpio -P
sudo grub-mkconfig -o /boot/grub/grub.cfg
sudo reboot
sudo systemctl enable nvidia-persistenced.service
```
