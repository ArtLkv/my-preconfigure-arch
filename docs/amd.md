# Install the AMD driver

1. Install the driver.

```sh
yay -S mesa mesa-vdpau vulkan-radeon glu vulkan-icd-loader
yay -S lib32-mesa lib32-mesa-vdpau lib32-vulkan-radeon lib32-glu lib32-vulkan-icd-loader
```

If you use the X Server:

```sh
yay -S xf86-video-amdgpu
```

2. Re-configure the system.

If `Southern Islands(SI)`:

```sh
sudo nvim /etc/default/grub # Add "radeon.si_support=0 amdgpu.si_support=1" into GRUB_CMDLINE_LINUX
```

If `Sea Islands(CIK)`:

```sh
sudo nvim /etc/default/grub # Add "radeon.cik_support=0 amdgpu.cik_support=1" into GRUB_CMDLINE_LINUX
```

And after:

```sh
sudo nvim /etc/mkinitcpio.conf # Add "amdgpu radeon" into MODULES, into end
cd ~/Downloads
curl -LO "https://gitlab.com/ArtLkv/my-preconfigure-arch/-/raw/etc/X11/xorg.conf.d/20-amdgpu.conf"
sudo mv 20-amdgpu.conf /etc/X11/xorg.conf.d/20-amdgpu.conf
sudo mkinitcpio -P linux
sudo grub-mkconfig -o /boot/grub/grub.cfg
```
