# Terminal

1. Install the terminal apps.

```sh
yay -S htop xh exa bat
```

2. Install the fisher(and some plugins).

```sh
cd ~/Downloads/
curl -LO "htps://gitlab.com/ArtLkv/my-preconfigure-arch/-/raw/main/arch/.config/fish/config.fish"
mv config.fish ~/.config/fish/config.fish
curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source && fisher install jorgebucaran/fisher
fisher install yo-goto/ggl.fish # Google Search
fisher install eth-p/fish-plugin-sudo # Sudo with Ctrl+S
```

3. Install NodeJS.

```sh
fisher install jorgebucaran/nvm.fish
nvm install lts
set --universal nvm_default_version vx.x.x
```
