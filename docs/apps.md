# Must-Have applications

1. Install the browser.

```sh
yay -S firefox
```

2. Install the screenshot tool.

```sh
yay -S flameshot
```

3. Install the media tools.

```sh
yay -S mpv # For video, audio
yay -S qimgv # For pictures
```

4. Install the messengers.

```sh
yay -S telegram-desktop discord
```

5. TimeShift.

```sh
yay -S timeshift
```
