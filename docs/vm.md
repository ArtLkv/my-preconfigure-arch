# Setup the VM's

1. Install the VirtualBox.

```sh
yay -S virtualbox virtualbox-host-dkms virtualbox-ext-oracle virtualbox-guest-iso
sudo gpasswd -a $USER vboxusers
sudo nvim /etc/modules-load.d/vbox.conf # Add "vboxdrv"
```
