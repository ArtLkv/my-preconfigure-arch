# My Arch configuration

**Tasks**: Gaming, DevOps

**Filesystem**: btrfs

**WM**: i3

**Browser**: Firefox

**Terminal**: Alacritty

**Text Editor**: Neovim

**Software**: AUR, flatpak, snap

**Theme**: Gruvbox

## Install

1. Check efi.

```sh
ls /sys/firmware/efi/efivars
```

2. Connect to the Wi-Fi(if needed).

```sh
iwctl --passphrase password station wlan0 connect wifi_name
```

Replace `password` and `wifi_name`, and also `wlan0` if is different.

3. Change timezone(if needed).

```sh
timedatectl set-timezone Europe/Moscow
```

Replace `Europe/Moscow` if needed.

4. Make partitions.

```sh
cfdisk /dev/current_disk
mkfs.btrfs -L arch -f /dev/linux_partition
mount /dev/disk/by-label/arch /mnt

btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@var
btrfs subvolume create /mnt/@opt
btrfs subvolume create /mnt/@tmp
umount /mnt

mount -o noatime,commit=120,compress=zstd,space_cache=v2,subvol=@ /dev/disk/by-label/arch /mnt
mkdir /mnt/{home,opt,tmp,var}
mount -o noatime,commit=120,compress=zstd,space_cache=v2,subvol=@home /dev/disk/by-label/arch /mnt/home
mount -o noatime,commit=120,compress=zstd,space_cache=v2,subvol=@opt /dev/disk/by-label/arch /mnt/opt
mount -o noatime,commit=120,compress=zstd,space_cache=v2,subvol=@tmp /dev/disk/by-label/arch /mnt/tmp
mount -o subvol=@var /dev/disk/by-label/arch /mnt/var

mkswap /dev/swap_partition
swapon /dev/swap_partition

mkdir /mnt/efi
mkfs.fat -F 32 /dev/efi_partition
mount /dev/efi_partition /mnt/efi
```

Replace `current_disk`, `linux_partition`, `swap_partition` and `efi_partition` to correct one.

5. Install the kernel, firmware and other packages.

```sh
pacstrap /mnt base base-devel linux-firmware # Main
pacstrap /mnt linux-zen linux-zen-headers # Kernel
pacstrap /mnt intel-ucode iucode-tool # Microcode for processor
pacstrap /mnt grub dosfstools mtools efibootmgr os-prober ntfs-3g # Boot loader(include DualBoot)
pacstrap /mnt btrfs-progs grub-btrfs # Btrfs
pacstrap /mnt sudo rsync which reflector ccache neofetch zip unzip # Must-have packages
pacstrap /mnt neovim git fish openssh fzf ripgrep # Development
pacstrap /mnt wpa_supplicant networkmanager acpid dialog dhcpcd cups cups-pdf tlp xdg-utils # Firmware and etc

```

Replace `intel-ucode` to `amd-ucode` if needed.

6. Enter into the system.

```sh
genfstab -U /mnt >> /mnt/etc/fstab
cat /mnt/efi/fstab
arch-chroot /mnt
```

6. Setup the locales.

```sh
loadkeys /usr/share/kbd/keymaps/i386/qwerty/lang.map.gz
echo "KEYMAP=lang" >> /etc/vconsole.conf
nvim /etc/locale.gen # And uncomment en_US.UTF-8 and your UTF-8 native language
locale-gen
curl -L "https://gitlab.com/ArtLkv/my-preconfigure-arch/-/raw/main/arch/etc/locale.conf" > /etc/locale.conf
nvim /etc/locale.conf # And replace ru_RU to your native language
ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
```

Replace `lang` to your native language. Also replace `Europe/Moscow` to your timezone.

7. Create the user.

```sh
passwd # Setup the password for root
useradd -m -g users -G wheel,storage,power -s /bin/fish username
passwd username # Setup the password for username
EDITOR=nvim visudo # Uncomment "%wheel ALL=(ALL:ALL) ALL"
echo "username" > /etc/hostname
nvim /etc/hosts # Add the three lines below
# 127.0.0.1     localhost
# ::1           localhost
# 127.0.0.1     username.localdomain    localhost
```

8. Setup the bootloader.

```sh
nvim /etc/mkinitcpio.conf # And add into MODULES "btrfs"
nvim /etc/default/grub # Uncomment "GRUB_DISABLE_OS_PROBER=false"
mkinitcpio -P linux
grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=ARCH --recheck
grub-mkconfig -o /boot/grub/grub.cfg
```

9. Enable the services.

```sh
systemctl enable NetworkManager.service
systemctl enable cups.service
systemctl enable tlp.service
systemctl enable acpid
systemctl enable dhcpcd
systemctl enable sshd
systemctl enable reflector.timer
systemctl enable fstrim.timer
```

10. Reboot the system.

```sh
exit
umount -lR /mnt
reboot
```


## Configure

1. Connect to the Wi-Fi(if needed).

```sh
sudo nmcli dev wifi connect wifi_name password wifi_password
```

Replace `wifi_name` and `wifi_password`.

2. Create the user folders.

```sh
mkdir ~/{Downloads,Documents,Videos,Pictures,Music,Projects}
```

3. Pacman - arch package manager

```sh
sudo vim /etc/pacman.conf # Uncomment "Color" and [multilib] block
sudo reflector --verbose --country 'Germany' -l 25 --sort rate --save /etc/pacman.d/mirrorlist
sudo nvim /etc/pacman.d/mirrorlist # Add the two lines below
# Server = http://mirror.yandex.ru/archlinux/$repo/os/$arch
# Server = https://mirror.yandex.ru/archlinux/$repo/os/$arch
sudo pacman -Suy

cd ~/Downloads
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```

4. Download the fonts.

```sh
yay -S ttf-dejavu noto-fonts
cd ~/Downloads
curl -LO "https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/CascadiaCode.zip"
unzip CascadiaCode.zip
sudo mkdir -p /usr/share/fonts/CascadiaCode
sudo cp ~/Downloads/*.ttf /usr/share/fonts/CascadiaCode
cd ~/Downloads/
git clone https://gitlab.com/ArtLkv/ms-fonts.git
cd ms-fonts
./install.sh
sudo fc-cache --force
```

5. Setup the keyboard.

```sh
cd ~/Downloads
curl -LO "https://gitlab.com/ArtLkv/my-preconfigure-arch/-/raw/arch/etc/X11/xorg.conf.d/00-keyboard.conf"
sudo mkdir -p /etc/X11/xorg.conf.d/
sudo mv 00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
sudo nvim /etc/X11/xorg.conf.d/00-keyboard.conf # And change Option XkbLayout to english and your native language
```

6. Setup the bluetooth.

```sh
yay -S bluez bluez-utils
sudo systemctl enable bluetooth.service
sudo systemctl start bluetooth.service
```

7. Setup the audio.

```sh
yay -S pipewire pipewire-alsa pipewire-pulse pipewire-jack wireplumber pavucontrol alsa-utils
yay -S lib32-pipewire lib32-pipewire-jack
mkdir -p ~/.config/pipewire/media-session.d/
cp /usr/share/pipewire/*.conf ~/.config/pipewire/
sudo nvim ~/.config/pipewire/pipewire.conf # Add into default.clock.allowed-rates "44100"
systemctl --user enable pipewire.service
systemctl --user enable pipewire-pulse.service
```

8. Setup the video driver.

For [Nvidia](./docs/nvidia.md), for [AMD](./docs/amd.md).

9. Another drivers.

- [Tp-Link: Wireless USB Adapter](./docs/tplink.md)

10. Install the DE.

[Install and configure i3](./docs/i3.md)

11. Install software.

- [Must-Have applications](./docs/apps.md)
- [Flatpak and snap](./docs/alt_pkgs.md)
- [Office packages](./docs/office.md)
- [Terminal](./docs/terminal.md)
- [DevOps](./docs/devops.md)

12. Extras

- [Firewall](./docs/firewall.md)
- [Virtualization](./docs/vm.md)
- [Ventoy](./docs/ventoy.md)
- [BalenaEtcher](./docs/etcher.md)
