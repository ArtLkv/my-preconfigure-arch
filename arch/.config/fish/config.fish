if status is-interactive
    bind \cs '__ethp_commandline_toggle_sudo'

    set -x EDITOR 'nvim'

    alias cls="clear"
    alias v="nvim"
    alias l="exa -l --icons --git -a"
    alias lt="exa --tree --level=2 --long --icons --git"
    alias http="xh"
end

